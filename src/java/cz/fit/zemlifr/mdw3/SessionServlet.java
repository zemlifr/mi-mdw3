/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.zemlifr.mdw3;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author frantisek
 */
public class SessionServlet extends HttpServlet
{
    public static final String STATE_KEY = "state";
    public static final String NEW = "NEW";
    public static final String PAYMENT = "PAYMENT";
    public static final String COMPLETED = "COMPLETED";
    public static final String COOKIEID = "SESSIONCOOKIE";
    
    protected void processResponse(TransitionResult result, String state, HttpServletResponse response)
            throws ServletException, IOException {
response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            switch(result)
            {
                case NEW_SESSION_STARTED:
                {
                    out.println("New Session started.");
                    break;
                }
                
                case STATE_UNCHANGED:
                {
                    out.println("State unchanged.");
                    break;
                }
                case FINAL:
                {
                    out.println("Request completed.");
                    break;
                }
                case STATE_CHANGE:
                {
                    out.println("State changed to: "+state+".");
                    break;
                }
                case INVALID_TRANSITION:
                {
                    out.println("Invalid transition.");
                    break;
                }
                    
            }
        } finally 
        {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        String state = request.getParameter(STATE_KEY);
        if(state == null)
            state = "";
        HttpSession session = loadSession(request, response);
        TransitionResult ret = checkState(session, state);
        
        processResponse(ret,state,response);
        
        
        if(ret == TransitionResult.FINAL)
        {
            Cookie cookie = new Cookie(COOKIEID, null);
            cookie.setPath("/");
            cookie.setMaxAge(0);
            response.addCookie(cookie);
            session.invalidate();
            return;
        }
        
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        throw new ServletException();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private TransitionResult checkState(HttpSession session, String state) 
    {
        Object sessionState = session.getAttribute(STATE_KEY);
        if((sessionState == null && state.isEmpty()) || state.isEmpty() )
        {
            session.setAttribute(STATE_KEY, NEW);
            return TransitionResult.NEW_SESSION_STARTED;
        }
        
        String s = (String)sessionState;
        if(s!=null)
        {
        if(s.equals(NEW))
        {
            if(state.equals(PAYMENT))
            {
                session.setAttribute(STATE_KEY, state);
                return TransitionResult.STATE_CHANGE;
            }
            else if(state.equals(NEW))
            {
                return TransitionResult.STATE_UNCHANGED;
            }
        }
        else if(s.equals(PAYMENT))
        {
            if(state.equals(COMPLETED))
            {
                session.setAttribute(STATE_KEY, state);
                return TransitionResult.FINAL;
            }
            else if(state.equals(PAYMENT))
            {
                return TransitionResult.STATE_UNCHANGED;
            }
        }
        }
        return TransitionResult.INVALID_TRANSITION;
    }

    /**
     * Thanks to http://stackoverflow.com/questions/16099752/session-or-cookie-confusion
     * @param request
     * @return 
     */
    private HttpSession loadSession(HttpServletRequest request, HttpServletResponse response) 
    {
        HttpSession session = null;

        String storedSessionId = getCookieValue(request, COOKIEID);
        
        Map<String, HttpSession> sessions = (Map<String, HttpSession>) getServletContext().getAttribute("sessions");
        
        // If a persistent session cookie has been created
        if (storedSessionId != null)
        {
            session = sessions.get(storedSessionId);
        }
        // Otherwise a session has not been created
        if (session == null)
        {
                    // Create a new session
            session = request.getSession();
            sessions.put(session.getId(), session);
            Cookie cookie = new Cookie(COOKIEID, session.getId());
            cookie.setPath("/");
            cookie.setMaxAge(1800);
            response.addCookie(cookie);
        }
        
        return session;
    }
    
    private String getCookieValue(HttpServletRequest request, String name)
    {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) 
            {
                for (Cookie cookie : cookies) 
                {
                    if (name.equals(cookie.getName())) 
                    {
                        return cookie.getValue();
                    }
                }
            }       
            return null;
    }

}
