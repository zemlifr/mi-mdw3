/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.zemlifr.mdw3;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author frantisek
 */
public class SessionListener implements HttpSessionListener
{

    public void sessionCreated(HttpSessionEvent se) 
    {
        HttpSession session = se.getSession();
        Map<String, HttpSession> sessions = (HashMap<String, HttpSession>)session.getServletContext().getAttribute("sessions");
        sessions.put(session.getId(), session);
    }

    public void sessionDestroyed(HttpSessionEvent se) 
    {
        HttpSession session = se.getSession();
        Map<String, HttpSession> sessions = (HashMap<String, HttpSession>)session.getServletContext().getAttribute("sessions");
        sessions.remove(session.getId());
    }
    
}
