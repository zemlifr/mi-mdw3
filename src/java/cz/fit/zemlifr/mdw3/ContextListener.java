/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.zemlifr.mdw3;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;

/**
 * Stores session map into context
 * @author frantisek
 */
public class ContextListener implements ServletContextListener
{

    public void contextInitialized(ServletContextEvent sce) 
    {
        Map<String, HttpSession> sessions = new HashMap<String, HttpSession>();
        sce.getServletContext().setAttribute("sessions", sessions);
    }

    public void contextDestroyed(ServletContextEvent sce) 
    {
    }
    
}
