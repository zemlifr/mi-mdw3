/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.zemlifr.mdw3;

/**
 *
 * @author frantisek
 */
public enum TransitionResult 
{
    FINAL,
    STATE_CHANGE,
    INVALID_TRANSITION,
    NEW_SESSION_STARTED,
    STATE_UNCHANGED
}
